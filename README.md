# NordVPN Utility Module for Py3status
## Installation
* TODO
## Usage
* TODO
## Features
* Shows your current IP
* Determines based on the current IP if it points to NordVPN servers this will be shown as 'SECURE' connection all non-NordVPN IP's will be treated 'UNSECURE'
* Color coded based on the current status ('SECURE'/'UNSECURE')
* On-demand updating via clicking the plugin
## Todo
* Add load information and server name?
	* Load info can be queried from the url https://api.nordvpn.com/server/stats/id2.nordvpn.com
    * Maybe add notify to on_click to inform user about the load and server name?
* Add changing of the current server to lower load server via scroll via helper script (up or down)
* Add killing openvpn via helper script (right click)
* Add notification once the current server load is above 50
* Add housekeeping to JSON files
* Add getcwd to determine the homefolder (and the default config location?)
* Change the configpath to hello_nordvpn2_configs from nordvpn_configs
