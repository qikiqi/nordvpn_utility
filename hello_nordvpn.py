# -*- coding: utf-8 -*-
"""
Simple module that queries NordVPN API to
    retrieve IP and if the IP is one of their servers.

FEATURES:
    - Shows your current IP
    - Determines based on the current IP
        if it points to NordVPN servers
        this will be shown as 'SECURE' connection
        all non-NordVPN IP's will be treated 'UNSECURE'
    - Color coded based on the current status ('SECURE'/'UNSECURE')
    - On-demand updating via clicking the plugin

TODO:
    - ADD REFRESHING THE OVPN CONFS
    - Add load information and server name?
        - Load info can be queried from the url https://api.nordvpn.com/server/stats/id2.nordvpn.com
        - Maybe add notify to on_click to inform user about the load and server name?
    - Add changing of the current server to lower load server via scroll via helper script (up or down)
    - Add killing openvpn via helper script (right click)
    - Add notification once the current server load is above 50
    - Add housekeeping to JSON files
    - Add getcwd to determine the homefolder (and the default config location?)
    - Change the configpath to hello_nordvpn2_configs from nordvpn_configs

BUGS:
    - If no connection, don't set the security_boolean
    - If from SECURE to OFFLINE, the OFFLINE color stays COLOR_GOOD instead of COLOR_BAD
    - If transitioning from SECURE to UNSECURE, why SECURE/UNSECURE
        lags one update behind IP?

NOTE: The self.py3 module helper has to be explicitly passed from the
        non-underscore functions to the underscore functions
        (as long as better solution is implemented, maybe assign again to self?)
"""
import json
import os
from datetime import date

class Py3status:

    def __init__(self):
        self.api_headers = self._get_api_headers()
        self.full_text = 'Initializing NordVPN status'
        self.homepath = '/home/x230/.i3/py3status/'
        self.configpath = self.homepath + 'nordvpn_configs/'
        os.makedirs(self.configpath, exist_ok=True)
        self.user_ip = "0.0.0.0"
        self.server_load = -1

    def hello_nordvpn(self):
        security_boolean = self._get_secure_info(self.py3)
        security_string = " " if security_boolean else " "
        security_color = self.py3.COLOR_GOOD if security_boolean is True else self.py3.COLOR_BAD
        try:
            full_text = security_string
            full_text += self._get_user_info(self.py3)
        except Exception:
            full_text = "OFFLINE"
        return {
            'full_text': full_text,
            'color': security_color,
            'cached_until': self.py3.time_in(seconds=60)
        }

    ## TODO: UNDER CONSTRUCTION
    def on_click(self, event):
        """
        event will be a dict like
        {'y': 13, 'x': 1737, 'button': 1, 'name': 'example', 'instance': 'first'}
        """
        #button = event['button']
        # update our output (self.full_text)
        #format_string = 'You pressed button {button}'
        #data = {'button': button}
        #self.full_text = self.py3.safe_format(format_string, data)
        #self.full_text = "LOL"
        self.py3.notify_user("Load: {}".format(self.server_load), title="Server Name {}".format(os.getcwd()))
        #return {
        #    'full_text': "LOL",
        #    'color': self.py3.COLOR_BAD,
        #    'cached_until': self.py3.time_in(seconds=5)
        #}

        # Our modules update methods will get called automatically.

    ## Determine if the current IP is NordVPN server or not
    #
    # @param self pointer to self
    # @param py3 module helper
    #
    # @return connection_secured boolean to determine connections security
    def _get_secure_info(self, py3):
        server_info = self._get_server_info(py3)
        connection_secured = self._get_connection_status(server_info)
        return connection_secured

    ## Determine if the server info has to be downloaded or retrieved from
    #   already existing file and return the server info
    #
    # @param self pointer to self
    # @param py3 module helper
    #
    # @return server_info the data containing all the info about the servers
    def _get_server_info(self, py3):
        today_yyyymmdd = date.today().strftime('%Y%m%d')
        today_filepath = self.configpath + 'nordvpn_servers_' + today_yyyymmdd + '.json'
        if os.path.isfile(today_filepath):
            with open(today_filepath) as f:
                server_info = json.load(f)
        else:
            # Download the server info JSON
            server_info = self._download_server_info(py3)
            # Write the file
            self._write_server_info(today_filepath, server_info)
        return server_info

    ## Download the server info for today if it can't be found
    #
    # @param self pointer to self
    # @param py3 module helper
    #
    # @return server_info the JSON of the server info
    def _download_server_info(self, py3):
        server_info_url = self._get_server_info_url()
        api_headers = self.api_headers
        server_info = self._make_request(py3, server_info_url, api_headers)
        return server_info

    ## Write the server info with todays filename to the config path
    #
    # @param self pointer to self
    # @param today_filepath the filepath and filename of the JSON file to write
    # @param server_info the JSON of the server info
    def _write_server_info(self, today_filepath, server_info):
        with open(today_filepath, 'w') as outfile:
            #outfile.write(json.dumps(server_info))
            json.dump(server_info, outfile)


    ## Get the status of the connection security
    #
    # @param self pointer to self
    # @param server_info the data containing all the info about the servers
    #
    # @return connection_secured boolean to indicate the server connection status
    def _get_connection_status(self, server_info):
        for item in server_info:
            ip = item['ip_address']
            if ip == self.user_ip:
                self.server_load = item['load']
                return True
        return False

    ## Get the IP of the user
    #
    # @param self pointer to self
    # @param py3 module helper
    #
    # @return string containing the information
    def _get_user_info(self, py3):
        user_info_url = self._get_user_info_url()
        api_headers = self.api_headers
        user_info = self._make_request(py3, user_info_url, api_headers)
        self.user_ip = user_info
        return user_info

    ## Make a request using the py3 request helper module
    #
    # @param self pointer to self
    # @param py3 module helper
    # @param url the url of the API endpoint for server data
    # @param headers the api headers that the API endpoint requires
    #
    # @return string containing the information
    def _make_request(self, py3, url, headers):
        # NOTE: We excpect error, which is caught in the main
        response_text = py3.request(url, headers)
        response_text = response_text.text
        try:
            return json.loads(response_text)
        except ValueError:
            return response_text

    ## Returns the API endpoint for the user IP info
    #
    # @param self pointer to self
    #
    # @return the API endpoint
    def _get_user_info_url(self):
        return 'https://api.nordvpn.com/user/address'

    ## Returns the API endpoint for the server info
    #
    # @param self pointer to self
    #
    # @return the API endpoint
    def _get_server_info_url(self):
        return 'https://api.nordvpn.com/server'

    ## Returns the API headers that the endpoint requires
    #
    # @param self pointer to self
    #
    # @return headers the api headers that the API endpoint requires
    def _get_api_headers(self):
        headers = {
            'User-Agent': 'NordVPN_Client_5.56.780.0',
            'Host': 'api.nordvpn.com',
            'Connection': 'Close'
        }
        return headers

if __name__ == "__main__":
    """
    Run module in test mode.
    """
    from py3status.module_test import module_test
    module_test(Py3status)
